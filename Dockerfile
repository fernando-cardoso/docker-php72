FROM openshift/base-centos7

MAINTAINER Vinicius Figueiredo <infra@pagzoop.com>

RUN yum install -y epel-release && \
    rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm && \
    yum install -y php72w php72w-cli php72w-mbstring php72w-mcrypt \
                   php72w-mysql php72w-xml php72w-fpm nginx supervisor && \
    yum clean all -y

ADD conf/nginx.conf /etc/nginx/nginx.conf
ADD conf/php-fpm.conf /etc/php-fpm.conf
ADD conf/www.conf /etc/php-fpm.d/www.conf
ADD conf/supervisord.conf /etc/supervisord.conf

RUN ln -sf /dev/stdout /tmp/access.log && \
    ln -sf /dev/stderr /tmp/error.log && \
    mkdir -p /var/log/nginx && \
    touch /tmp/{access,error}.log && \ 
    chown -R default. /var/lib && \
    mkdir /app && \
    chmod -R 777 /var/log /var/lib /usr/share/nginx

ADD src/index.php /app/index.php

EXPOSE 8080

WORKDIR /tmp

USER default
CMD [ "supervisord", "-n", "-c", "/etc/supervisord.conf" ]
