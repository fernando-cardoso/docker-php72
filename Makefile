IMAGE_NAME = zoop/php72

.PHONY: build
build:
	docker build -t $(IMAGE_NAME) .
