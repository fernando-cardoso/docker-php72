# Docker Image: PHP 7.2

Imagem base para projetos PHP com Docker:

* PHP 7.2
* PHP-FPM
* Nginx

## Building

    $ make build
